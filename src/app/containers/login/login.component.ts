import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loading: boolean = false;

  loginForm: FormGroup = new FormGroup({
    trainerName: new FormControl('', [
      Validators.required,
      Validators.minLength(2),
      Validators.maxLength(20)
    ])
  })
  constructor() { }

  ngOnInit(): void {
  }

  get trainerName(): AbstractControl {
    return this.loginForm.get('trainerName')
  }

  onStartClick() {
    console.log(this.loginForm.value)
    this.loading = true
    setTimeout(() => {
      this.loading = false
    }, 2000);
  }

}
